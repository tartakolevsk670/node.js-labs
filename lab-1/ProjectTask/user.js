const fs = require("fs")

function add(language) {

    let userJSON =  fs.readFileSync("user.json", "utf-8")

    let user = JSON.parse(userJSON);

    for(let elem of user.languages)
    {
        if(elem.title === language.title)
        {
            console.log("Can't add language, item exists.")
            return;
        }
    }

        user.languages.push(language)


        fs.writeFileSync("user.json", JSON.stringify(user))
        console.log("Language added success.")
}

function remove(title) {
    let userJSON =  fs.readFileSync("user.json", "utf-8")

    let user = JSON.parse(userJSON);
    const index = user.languages.findIndex(item => item.title === title.title);
    if(index !== -1)
    {
        user.languages.splice(index,1);
        fs.writeFileSync("user.json", JSON.stringify(user));
        console.log("Removed is success");
        console.log(index)
    }else
        console.log("No title.")
    console.log(index)



}

function list() {
    let userJSON =  fs.readFileSync("user.json", "utf-8")
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);
    console.log(user.languages);
}

function read(title) {
    let userJSON =  fs.readFileSync("user.json", "utf-8")
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);

    for(let elem of user.languages)
    {
        if(elem.title === title.title) {
            console.log(elem);
            return;
        }
    }
    console.log("No item.")


}

module.exports = {add, remove, list, read}