const user = require("./user")
const yargs = require('yargs/yargs');


yargs(process.argv.slice(2))
    .command({
        command: 'list',
        aliases: ['ls'],
        desc: 'Get the list of languages',
        handler: (argv) => {
           user.list();
        }
    })
    .command({
        command: 'add',
        desc: 'Add new language',
        builder: {
            title: {
                type: 'string',
                demandOption : true,
                describe: 'Language title'
            },
            level:
                {
                    describe: 'Level of Knowledge',
                    demandOption: true,
                    type: 'string'
                }
        },
        handler: (argv) => {
            user.add({title: argv.title, level: argv.level})
        }
    })
    .command({
        command: 'remove',
        desc: 'Remove language',
        builder: {
            title: {
                type: 'string',
                demandOption : true,
                describe: 'Language title'
            },
        },
        handler: (argv) => {
            user.remove({title: argv.title})
        }
    })
    .command({
        command: 'read',
        desc: 'Read language',
        builder: {
            title: {
                type: 'string',
                demandOption : true,
                describe: 'Language title'
            },
        },
        handler: (argv) => {
            user.read({title: argv.title})
        }
    }).parse()