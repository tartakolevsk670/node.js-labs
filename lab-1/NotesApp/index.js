const lodash = require("lodash");
//Creates an array of elements split into groups the length of size
console.log(lodash.chunk(['a', 'b', 'c', 'd'], 2));
//Creates a new array concatenating array with any additional arrays and/or values.
var array = [1];
var other = lodash.concat(array, 2, [3], [[4]]);
console.log(other)
//Creates an array of array values not included in the other given arrays using SameValueZero for equality comparisons.
console.log(lodash.difference([2, 1,3,4], [2, 3]));
//Fills elements of array with value from start up to, but not including, end.
console.log(lodash.fill([4, 6, 8, 10], '*', 1, 3));

//Invokes func after wait milliseconds. Any additional arguments are provided to func when it's invoked.
lodash.delay(function(text) {
    console.log(text);
}, 1000, 'later');

