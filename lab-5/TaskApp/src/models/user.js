const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

const userSchema = new mongoose.Schema({
    tokens:[
        {token:
                {
                  type: String,
                  required: true
                }}
    ],
    email:{
        type: String,
        required: true,
        validate(value) {
            if(!validator.isEmail(value))
                throw new Error("Email is invalid")
        }

    },
    password:{
        type: String,
        required: true,
        minValue:7,
        trim: true,
        validate(value) {
            if(value === "password")
                throw new Error("Try another password")
        }
    },
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        default: 20,
        validate(value)
        {
            if(value<0)
                throw new Error("Age must be a positive number")
        }
    }
},{toJSON: {virtuals:true}, toObject:{virtuals:true}})

userSchema.pre('save', async function(next)
{
    const user = this;
    if(user.isModified('password'))
    {
        user.password = await bcrypt.hash(user.password,8);
    }
    next();
});

userSchema.statics.findOneByCredentials = async (email,password)=>
{
    const user = await User.findOne({email});
    if(!user)
    {
        throw new Error("Incorrect email.");
    }
    const isMatch = await bcrypt.compare(password,user.password);
    if(!isMatch)
    {
        throw new Error("Incorrect password.");
    }
    return user;
};
userSchema.methods.generateAuthToken = async function()
{
    const user = this;
    const token = jwt.sign({_id:user._id.toString()},'key111')
    user.tokens = user.tokens.concat({token})
    await user.save();
    return token;
};

userSchema.virtual(
    "tasks", {
        ref:"Task",
        localField: "_id",
        foreignField:"owner"
    }
)

userSchema.methods.toJSON = function()
{
    const userCopy = this.toObject();
    delete  userCopy.password;
    delete  userCopy.tokens;
    return userCopy;
}

const User = mongoose.model("User", userSchema)
module.exports = User