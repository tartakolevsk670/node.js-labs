const express = require('express')
const hbs = require("hbs")
const fetch = require('node-fetch')
const {response} = require("express");
const app = express()
const port= 3000
const key = "b5018676b6c9e7d01aa7056fd2b9186d"

app.set("view engine",'hbs')
hbs.registerPartials(__dirname + 'views/partials')

app.get('/',(req, res) =>
{
    res.send('Hello World')
})

app.get('/weather/:city?',async ( req, res) =>
{
    let city = req.query['city'] || req.params['city'];
    if(city !== undefined)
    {
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`;
            let responseAPI = await fetch(url);
            let weather = await responseAPI.json();
            let weatherObj = {
                weather: weather,
                city: city
            }
            res.render('weather.hbs', {weatherObj});
    }
    else
        res.redirect('weather/Zhytomyr')


})

app.listen(port, ()=>
{
    console.log(`Example app listening ${port}`)
})