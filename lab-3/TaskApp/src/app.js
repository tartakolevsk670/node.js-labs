const express = require("express")
const mongoose = require("mongoose")
const User = require("./models/user")
const Task = require("./models/task")
require("dotenv").config(); //process.env

let url = process.env.MONGO_URL;
let port = process.env.PORT;
console.log(url)
console.log(port)
console.log("dfdfdfddfd-------")
mongoose.connect(url)

const app = express();
app.use(express.json());//body-pars, автоматично запускається для парсингу тіла сторінки

app.get("/users", async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        res.send(error.message);
    }
})
app.get("/task/:id", async (req, res) => {
    try {
        const task = await Task.findOne({ _id: `${req.params.id}` });
        res.json(task);
    } catch (error) {
        res.send(error.message);
    }
})

app.get("/user/:id", async (req, res) => {
    try {
        const user = await User.findOne({ _id: `${req.params.id}` });
        res.json(user);
    } catch (error) {
        res.send(error.message);
    }
})

app.get("/tasks", async (req, res) => {
    try {
        const tasks = await Task.find();
        res.json(tasks);
    } catch (error) {
        res.send(error.message);
    }
})

app.post("/tasks", async (req,res)=>
{
    try{
        const task = new Task(req.body); //Створюємо екземпляр моделі з даними тіла запиту
        await task.save();
        res.json(task);
    }
    catch (error)
    {
        res.send(error.message)
    }
})

app.post("/users", async (req, res) => {
    try {
        const user = new User(req.body); //Створюємо екземпляр моделі з даними тіла запиту
        await user.save();
        res.json(user);
    } catch (error) {
        res.send(error.message);
    }
})


app.delete("/users", async (req, res) => {
    try {
        await User.deleteMany();
        res.send("Success.")
    } catch (error) {
        res.send(error.message);
    }
})
app.delete("/tasks", async (req, res) => {
    try {
        await Task.deleteMany();
        res.send("Success.")
    } catch (error) {
        res.send(error.message);
    }
})

app.delete("/user/:id", async (req, res) => {
    try {
        await User.deleteOne({_id:req.params.id});
        res.send(`Success delete obj with id:${req.params.id}`)
    } catch (error) {
        res.send(error.message);
    }
})
app.delete("/task/:id", async (req, res) => {
    try {
        await Task.deleteOne({_id:req.params.id});
        res.send(`Success delete obj with id:${req.params.id}`)
    } catch (error) {
        res.send(error.message);
    }
})


app.patch("/user/:id", async (req, res) => {
    try {
        let id = req.params.id;
        let post = req.body;
      //  const user = await User.findById(id);
        const query = { _id:  `${id}` };
        if(post.email !== null)
           await User.findOneAndUpdate(query, { $set: { email: post.email }})
        if(post.password !== null)
            await User.findOneAndUpdate(query, { $set: { password: post.password }})
        if(post.firstName !== null)
            await User.findOneAndUpdate(query, { $set: { firstName: post.firstName }})
        if(post.lastName !== null)
            await User.findOneAndUpdate(query, { $set: { lastName: post.lastName }})
        if(post.age !== null)
            await User.findOneAndUpdate(query, { $set: { age: post.age }})

        res.send(`Success update obj with id:${req.params.id}`)
    } catch (error) {
        res.send(error.message);
    }
})
app.patch("/task/:id", async (req, res) => {
    try {
        let id = req.params.id;
        let post = req.body;
        //  const user = await User.findById(id);
        const query = { _id:  `${id}` };
        if(post.title !== null)
            await Task.findOneAndUpdate(query, { $set: { title: post.title }})
        if(post.description !== null)
            await Task.findOneAndUpdate(query, { $set: { description: post.description }})
        if(post.completed !== null)
            await Task.findOneAndUpdate(query, { $set: { completed: post.completed }})

        res.send(`Success update obj with id:${req.params.id}`)
    } catch (error) {
        res.send(error.message);
    }
})



app.listen(port, () => {
    console.log(`Server is listening on ${port} port`)
})