const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcrypt")

const userSchema = new mongoose.Schema({
    email:{
        type: String,
        required: true,
        validate(value) {
            if(!validator.isEmail(value))
                throw new Error("Email is invalid")
        }

    },
    password:{
        type: String,
        required: true,
        minValue:7,
        trim: true,
        validate(value) {
            if(value === "password")
                throw new Error("Try another password")
        }
    },
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        default: 20,
        validate(value)
        {
            if(value<0)
                throw new Error("Age must be a positive number")
        }
    }
})

userSchema.pre('save', async function(next)
{
    const user = this;
    if(user.isModified('password'))
    {
        user.password = await bcrypt.hash(user.password,8);
    }
    next();
});


const User = mongoose.model("User", userSchema)
module.exports = User